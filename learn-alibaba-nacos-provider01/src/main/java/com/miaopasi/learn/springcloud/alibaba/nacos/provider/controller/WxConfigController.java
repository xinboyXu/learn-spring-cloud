package com.miaopasi.learn.springcloud.alibaba.nacos.provider.controller;

import com.miaopasi.learn.springcloud.alibaba.nacos.provider.service.WxConfigService;
import com.miaopasi.learn.springcloud.common.pojo.WxConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信配置类控制层
 *
 * @author lixin
 */
@RestController
@RequestMapping("/alibaba/provider/wxConfig")
public class WxConfigController {

    private final WxConfigService wxConfigService;

    @Autowired
    public WxConfigController(WxConfigService wxConfigService) {
        this.wxConfigService = wxConfigService;
    }

    @GetMapping("find")
    public WxConfig find(
            @RequestParam(name = "appId", required = false, defaultValue = "") String appId
    ) {
        System.out.println("接口被请求");
        return wxConfigService.findByAppId(appId);
    }

}
