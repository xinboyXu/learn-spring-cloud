package com.miaopasi.learn.springcloud.stream.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 服务消费者
 *
 * @author lixin
 */
@SpringBootApplication
@EnableEurekaClient
public class StreamConsumerMain {
    public static void main(String[] args) {
        SpringApplication.run(StreamConsumerMain.class, args);
    }
}
