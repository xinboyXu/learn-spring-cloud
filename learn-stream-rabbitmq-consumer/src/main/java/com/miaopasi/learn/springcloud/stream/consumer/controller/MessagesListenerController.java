package com.miaopasi.learn.springcloud.stream.consumer.controller;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * 消息消费者
 *
 * @author lixin
 */
@Component
@EnableBinding(Sink.class)
public class MessagesListenerController {

    /*** 监听消息 */
    @StreamListener(Sink.INPUT)
    public void input(Message<String> message) {
        System.out.println("消息消费者接收到新消息：" + message.getPayload());
    }
}
