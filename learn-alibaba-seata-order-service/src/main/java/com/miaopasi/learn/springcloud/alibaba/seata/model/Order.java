package com.miaopasi.learn.springcloud.alibaba.seata.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 实体类
 * @author lixin
 */
public class Order implements Serializable {
    private Long id;
    private Long userId;
    private Long productId;
    private Integer count;
    private BigDecimal money;
    private Integer status;

    public Order() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public static final class OrderBuilder {
        private Long id;
        private Long userId;
        private Long productId;
        private Integer count;
        private BigDecimal money;
        private Integer status;

        private OrderBuilder() {
        }

        public static OrderBuilder anOrder() {
            return new OrderBuilder();
        }

        public OrderBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public OrderBuilder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public OrderBuilder productId(Long productId) {
            this.productId = productId;
            return this;
        }

        public OrderBuilder count(Integer count) {
            this.count = count;
            return this;
        }

        public OrderBuilder money(BigDecimal money) {
            this.money = money;
            return this;
        }

        public OrderBuilder status(Integer status) {
            this.status = status;
            return this;
        }

        public Order build() {
            Order order = new Order();
            order.setId(id);
            order.setUserId(userId);
            order.setProductId(productId);
            order.setCount(count);
            order.setMoney(money);
            order.setStatus(status);
            return order;
        }
    }
}
