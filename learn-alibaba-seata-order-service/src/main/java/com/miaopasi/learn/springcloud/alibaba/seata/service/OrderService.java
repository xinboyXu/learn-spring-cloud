package com.miaopasi.learn.springcloud.alibaba.seata.service;

import com.miaopasi.learn.springcloud.alibaba.seata.model.Order;

/**
 * 订单Service
 *
 * @author lixin
 */
public interface OrderService {

    /**
     * 创建订单
     *
     * @param order 订单信息
     */
    void create(Order order);
}
