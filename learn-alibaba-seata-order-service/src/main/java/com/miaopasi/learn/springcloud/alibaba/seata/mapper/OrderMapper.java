package com.miaopasi.learn.springcloud.alibaba.seata.mapper;

import com.miaopasi.learn.springcloud.alibaba.seata.model.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 订单Mapper
 *
 * @author lixin
 */
@Component
@Mapper
public interface OrderMapper {

    /**
     * 创建订单
     *
     * @param order 订单信息
     */
    void create(Order order);

    /**
     * 更新订单状态
     *
     * @param id     订单ID
     * @param status 订单状态
     */
    void updateStatus(@Param("id") Long id, @Param("status") Integer status);

}
