package com.miaopasi.learn.springcloud.alibaba.seata.service;

import com.miaopasi.learn.springcloud.common.pojo.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * 远程调用账户接口
 *
 * @author lixin
 */
@Component
@FeignClient(value = "seata-account-service")
public interface AccountService {

    /**
     * 扣减余额
     *
     * @param userId 用户ID
     * @param money  扣减余额
     * @return true成功
     */
    @PostMapping("/account/decrease")
    CommonResult<Boolean> decrease(
            @RequestParam("userId") Long userId,
            @RequestParam("money") BigDecimal money
    );

}
