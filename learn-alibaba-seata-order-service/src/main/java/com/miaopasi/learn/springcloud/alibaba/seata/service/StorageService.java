package com.miaopasi.learn.springcloud.alibaba.seata.service;

import com.miaopasi.learn.springcloud.common.pojo.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 远程调用库存接口
 *
 * @author lixin
 */
@Component
@FeignClient(value = "seata-storage-service")
public interface StorageService {

    /**
     * 扣减库存
     *
     * @param productId 库存ID
     * @param count     扣减数量
     * @return true成功
     */
    @PostMapping("/storage/decrease")
    CommonResult<Boolean> decrease(
            @RequestParam("productId") Long productId,
            @RequestParam("count") Integer count
    );

}
