package com.miaopasi.learn.springcloud.alibaba.seata.controller;

import com.miaopasi.learn.springcloud.alibaba.seata.model.Order;
import com.miaopasi.learn.springcloud.alibaba.seata.service.OrderService;
import com.miaopasi.learn.springcloud.common.pojo.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单控制层
 *
 * @author lixin
 */
@RestController
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/order/create")
    public CommonResult<Order> create(Order order) {
        orderService.create(order);
        return CommonResult.ok(order);
    }
}
