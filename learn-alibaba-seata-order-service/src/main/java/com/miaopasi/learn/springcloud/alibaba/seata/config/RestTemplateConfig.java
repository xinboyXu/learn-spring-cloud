package com.miaopasi.learn.springcloud.alibaba.seata.config;

import com.alibaba.cloud.sentinel.annotation.SentinelRestTemplate;
import com.netflix.loadbalancer.AvailabilityFilteringRule;
import com.netflix.loadbalancer.IRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * RestTemplate 配置
 *
 * @author lixin
 */
@Configuration
public class RestTemplateConfig {

    @Bean
    @SentinelRestTemplate
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /**
     * 配置负载均衡算法
     * RoundRobinRule：轮询
     * WeightedResponseTimeRule：轮询+响应时间权重
     * BestAvailableRule：轮询+并发
     * AvailabilityFilteringRule：随机+过滤掉一直连接失败的、并过滤掉高并发的
     * RandomRule：随机
     * RetryRule：重试
     */
    @Bean
    public IRule iRule() {
        return new AvailabilityFilteringRule();
    }

}
