package com.miaopasi.learn.springcloud.alibaba.seata.service.impl;

import cn.hutool.core.thread.ThreadUtil;
import com.miaopasi.learn.springcloud.alibaba.seata.mapper.AccountMapper;
import com.miaopasi.learn.springcloud.alibaba.seata.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

/**
 * 账户操作实现类
 *
 * @author lixin
 */
@Service
public class AccountServiceImpl implements AccountService {

    private final AccountMapper accountMapper;

    @Autowired
    public AccountServiceImpl(AccountMapper accountMapper) {
        this.accountMapper = accountMapper;
    }

    @Override
    public void decrease(Long userId, BigDecimal money) {
        ThreadUtil.sleep(10, TimeUnit.SECONDS);
        accountMapper.decrease(userId, money);
    }
}
