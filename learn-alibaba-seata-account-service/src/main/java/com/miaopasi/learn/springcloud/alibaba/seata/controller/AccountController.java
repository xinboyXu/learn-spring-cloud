package com.miaopasi.learn.springcloud.alibaba.seata.controller;

import com.miaopasi.learn.springcloud.alibaba.seata.service.AccountService;
import com.miaopasi.learn.springcloud.common.pojo.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * 账户控制层
 *
 * @author lixin
 */
@RestController
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/account/decrease")
    public CommonResult<Boolean> decrease(
            @RequestParam("userId") Long userId,
            @RequestParam("money") BigDecimal money
    ) {
        accountService.decrease(userId, money);
        return CommonResult.ok(Boolean.TRUE);
    }
}
