package com.miaopasi.learn.springcloud.alibaba.seata.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 账户Mapper
 *
 * @author lixin
 */
@Component
@Mapper
public interface AccountMapper {

    /**
     * 扣减余额
     *
     * @param userId 库存ID
     * @param money  扣减金额
     */
    void decrease(@Param("userId") Long userId, @Param("money") BigDecimal money);

}
