package com.miaopasi.learn.springcloud.alibaba.seata.service;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 账户接口
 *
 * @author lixin
 */
@Component
public interface AccountService {

    /**
     * 扣减余额
     *
     * @param userId 用户ID
     * @param money  扣减金额
     */
    void decrease(Long userId, BigDecimal money);

}
