package com.miaopasi.learn.springcloud.alibaba.seata.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 账户实体类
 *
 * @author lixin
 */
public class Account implements Serializable {
    private Long id;
    private Long userId;
    private BigDecimal total;
    private BigDecimal used;
    private BigDecimal residue;

    public Account() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getUsed() {
        return used;
    }

    public void setUsed(BigDecimal used) {
        this.used = used;
    }

    public BigDecimal getResidue() {
        return residue;
    }

    public void setResidue(BigDecimal residue) {
        this.residue = residue;
    }

    public static final class AccountBuilder {
        private final Account account;

        private AccountBuilder() {
            account = new Account();
        }

        public static AccountBuilder anAccount() {
            return new AccountBuilder();
        }

        public AccountBuilder id(Long id) {
            account.setId(id);
            return this;
        }

        public AccountBuilder userId(Long userId) {
            account.setUserId(userId);
            return this;
        }

        public AccountBuilder total(BigDecimal total) {
            account.setTotal(total);
            return this;
        }

        public AccountBuilder used(BigDecimal used) {
            account.setUsed(used);
            return this;
        }

        public AccountBuilder residue(BigDecimal residue) {
            account.setResidue(residue);
            return this;
        }

        public Account build() {
            return account;
        }
    }
}
