package com.miaopasi.learn.springcloud.zipkin.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 服务提供者启动类
 *
 * @author lixin
 */
@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker
public class ProviderZipkinMain {
    public static void main(String[] args) {
        SpringApplication.run(ProviderZipkinMain.class, args);
    }
}
