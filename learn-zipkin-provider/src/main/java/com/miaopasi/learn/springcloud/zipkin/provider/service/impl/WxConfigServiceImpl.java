package com.miaopasi.learn.springcloud.zipkin.provider.service.impl;

import com.miaopasi.learn.springcloud.common.pojo.WxConfig;
import com.miaopasi.learn.springcloud.zipkin.provider.service.WxConfigService;
import org.springframework.stereotype.Service;

/**
 * 微信配置接口实现
 *
 * @author lixin
 */
@Service
public class WxConfigServiceImpl implements WxConfigService {
    @Override
    public WxConfig findByAppId(String appId) {
        return WxConfig.WxConfigBuilder.aWxConfig()
                .appId(appId)
                .name("微信" + appId)
                .build();
    }
}
