package com.miaopasi.learn.springcloud.config.client.contorller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 读取配置中心配置的客户端
 *
 * @author lixin
 */
@RestController
@RequestMapping("/config")
// 开启刷新配置文件接口，刷新地址 POST http://localhost:8400/actuator/refresh
@RefreshScope
public class ConfigController {

    @Value("${config.info}")
    public String configInfo;

    @GetMapping("/info")
    public String info() {
        return configInfo;
    }
}
