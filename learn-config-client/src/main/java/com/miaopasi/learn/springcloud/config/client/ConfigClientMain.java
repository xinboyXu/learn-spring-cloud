package com.miaopasi.learn.springcloud.config.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 配置中心 客户端
 *
 * @author lixin
 */
@SpringBootApplication
public class ConfigClientMain {

    public static void main(String[] args) {
        SpringApplication.run(ConfigClientMain.class, args);
    }

}
