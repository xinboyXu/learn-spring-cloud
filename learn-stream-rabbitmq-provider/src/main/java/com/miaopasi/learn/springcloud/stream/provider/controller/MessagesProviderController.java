package com.miaopasi.learn.springcloud.stream.provider.controller;

import com.miaopasi.learn.springcloud.stream.provider.service.IMessagesProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 消息提供
 *
 * @author lixin
 */
@RestController
public class MessagesProviderController {

    private final IMessagesProvider messagesProvider;

    @Autowired
    public MessagesProviderController(IMessagesProvider messagesProvider) {
        this.messagesProvider = messagesProvider;
    }

    @GetMapping("send")
    public String send() {
        return messagesProvider.send();
    }
}
