package com.miaopasi.learn.springcloud.stream.provider.service;

/**
 * 发送消息接口
 * @author lixin
 */
public interface IMessagesProvider {

    /***
     * 发送消息
     * @return 响应消息
     */
    String send();

}
