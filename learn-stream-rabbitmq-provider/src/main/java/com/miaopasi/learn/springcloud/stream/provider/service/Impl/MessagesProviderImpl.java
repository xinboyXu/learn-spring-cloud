package com.miaopasi.learn.springcloud.stream.provider.service.Impl;

import com.miaopasi.learn.springcloud.stream.provider.service.IMessagesProvider;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 发送消息接口实现
 *
 * @author lixin
 */
@EnableBinding(Source.class)
public class MessagesProviderImpl implements IMessagesProvider {

    /*** 注入消息通道 */
    @Resource
    private MessageChannel output;

    @Override
    public String send() {
        String uuid = UUID.randomUUID().toString();
        // 构建发送消息对象
        Message<String> message = MessageBuilder.withPayload(uuid).build();
        // 使用消息通道发送消息对象
        output.send(message);
        return uuid;
    }

}
