package com.miaopasi.learn.springcloud.stream.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 服务提供者
 *
 * @author lixin
 */
@SpringBootApplication
@EnableEurekaClient
public class StreamProviderMain {
    public static void main(String[] args) {
        SpringApplication.run(StreamProviderMain.class, args);
    }
}
