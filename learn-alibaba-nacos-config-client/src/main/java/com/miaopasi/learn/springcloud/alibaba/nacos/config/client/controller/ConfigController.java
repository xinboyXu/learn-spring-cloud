package com.miaopasi.learn.springcloud.alibaba.nacos.config.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 配置中心
 *
 * @author lixin
 */
@RestController
@RefreshScope
public class ConfigController {

    @Value("${service.info}")
    public String configInfo;

    @GetMapping("info")
    public String find() {
        return configInfo;
    }

}
