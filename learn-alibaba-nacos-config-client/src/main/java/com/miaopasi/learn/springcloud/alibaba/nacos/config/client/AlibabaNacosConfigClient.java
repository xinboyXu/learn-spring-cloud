package com.miaopasi.learn.springcloud.alibaba.nacos.config.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Nacos 配置中心
 *
 * @author lixin
 */
@SpringBootApplication
@EnableDiscoveryClient
public class AlibabaNacosConfigClient {
    public static void main(String[] args) {
        SpringApplication.run(AlibabaNacosConfigClient.class, args);
    }
}
