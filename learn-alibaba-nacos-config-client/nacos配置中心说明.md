# nacos配置中心说明
* nacos配置中心服务端由nacos-server提供，只要到官网下载就可以使用，不用自己编写服务端。
* 客户端使用nacos配置中心的配置，需要在配置中心服务端进行配置。
* 客户端根据配置中心的 "Data ID"、"Group" 和 "NameSpace" 来获取配置文件的信息。
* Data ID 规则：${spring.application.name}-${spring.profile.active}.${spring.cloud.nacos.config.file-extension}
* 例如：learn-alibaba-nacos-config-client项目的DataID=alibaba-nacos-config-client-dev.yaml
* Group分组：如果Data ID是一样的，可以配置Group来区分。
* NameSpace命名空间：可以根据NameSpace命名空间来区分显示在页面的配置。NameSpace命名空间需要填写的是“命名空间ID”。

### 数据持久化到mysql数据库：
> 1. 单机部署使用嵌入数据库，集群部署需要存储到mysql，解决数据一致性问题。
> 2. 在安装包的conf中有nacos-mysql.sql，把数据库导入自己的mysql就完成数据库的创建，不用修改。
> 3. 修改nacos下的confapplication.properties，配置连接到mysql数据库。
```properties
spring.datasource.platform=mysql
db.num=1
db.url.0=jdbc:mysql://127.0.0.1:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
db.user.0=root
db.password.0=123456
```
> 注意：从嵌入数据库切换到mysql数据库，之前的数据需要手动导入。

### nacos配置中心集群
> 1. 集群最低要求：nginx *1、nacos-server *3、mysql *1
> 2. 在nacos的解压目录nacos/的conf目录下，有配置文件cluster.conf，请每行配置成ip:port。（请配置3个或3个以上节点，每个节点都需要配置）
```text
 # ip:port
 200.8.9.16:8848
 200.8.9.17:8848
 200.8.9.18:8848
```
> 3. 在nginx中配置负载均衡，confd/nacos-server.conf，使用127.0.0.1:8850访问nacos。 
```text
# nacos-server 负载均衡

upstream nacos_servers {
  ip_hash;
  server 127.0.0.1:8847;
  server 127.0.0.1:8848;
  server 127.0.0.1:8849;
}

server {
  listen 8850;
  server_name 127.0.0.1;
  location / {
    proxy_pass http://nacos_servers;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header REMOTE-HOST $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_connect_timeout 1s;  
    proxy_read_timeout 1s;  
    proxy_send_timeout 1s;
  }
}
```
