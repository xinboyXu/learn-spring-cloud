package com.miaopasi.learn.springcloud.zipkin.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * 服务消费者启动类
 *
 * @author lixin
 */
@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = "com.miaopasi.learn.springcloud")
@EnableFeignClients(basePackages = {"com.miaopasi.learn.springcloud"})
@EnableHystrixDashboard
public class ConsumerZipkinMain {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerZipkinMain.class, args);
    }

}
