package com.miaopasi.learn.springcloud.zipkin.consumer.controller;

import com.miaopasi.learn.springcloud.common.pojo.WxConfig;
import com.miaopasi.learn.springcloud.common.sevice.WxConfigClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信配置消费者
 *
 * @author lixin
 */
@RestController
@RequestMapping("/zipkin/consumer/wxConfig")
public class WxConfigConsumerController {

    private final WxConfigClientService wxConfigClientService;

    @Autowired
    public WxConfigConsumerController(WxConfigClientService wxConfigClientService) {
        this.wxConfigClientService = wxConfigClientService;
    }

    @GetMapping("find")
    public WxConfig find(
            @RequestParam(name = "appId", required = false, defaultValue = "") String appId
    ) {
        return this.wxConfigClientService.findByAppId(appId);
    }
}
