package com.miaopasi.learn.springcloud.gateway.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 使用配置中心的 服务网关
 *
 * @author lixin
 */
@SpringBootApplication
public class GatewayConfigMain {
    public static void main(String[] args) {
        SpringApplication.run(GatewayConfigMain.class, args);
    }
}
