package com.miaopasi.learn.springcloud.provider.hystrix.controller;

import com.miaopasi.learn.springcloud.common.pojo.Dept;
import com.miaopasi.learn.springcloud.provider.hystrix.service.DeptService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 部门接口
 *
 * @author lixin
 */
@RestController
@RequestMapping("/dept")
public class DeptController {

    private final DeptService deptService;

    @Autowired
    public DeptController(DeptService deptService) {
        this.deptService = deptService;
    }

    @HystrixCommand(fallbackMethod = "hystrixGet")
    @GetMapping("/get/{id}")
    public Dept get(@PathVariable Long id) {
        if (id <= 0) {
            throw new RuntimeException(id + "记录不存在");
        }
        return deptService.get(id);
    }

    /*** get的熔断方法 */
    public Dept hystrixGet(@PathVariable Long id) {
        return Dept.DeptBuilder.aDept()
                .id(id)
                .name("不存在" + id)
                .database("null")
                .build();
    }

}
