package com.miaopasi.learn.springcloud.provider.hystrix.config;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 注册Servlet
 *
 * @author lixin
 */
@Configuration
public class ServletRegistration {

    /*** Hystrix信息监控Servlet */
    @Bean
    public ServletRegistrationBean<HystrixMetricsStreamServlet> registrationBean() {
        return new ServletRegistrationBean<>(new HystrixMetricsStreamServlet(), "/actuator/hystrix.stream");
    }
}
