package com.miaopasi.learn.springcloud.alibaba.seata.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 库存Mapper
 *
 * @author lixin
 */
@Component
@Mapper
public interface StorageMapper {

    /**
     * 扣减库存
     *
     * @param id    库存ID
     * @param count 扣件数量
     */
    void decrease(@Param("id") Long id, @Param("count") Integer count);

}
