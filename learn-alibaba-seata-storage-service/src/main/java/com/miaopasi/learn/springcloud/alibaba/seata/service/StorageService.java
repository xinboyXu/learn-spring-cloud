package com.miaopasi.learn.springcloud.alibaba.seata.service;

import org.springframework.stereotype.Component;

/**
 * 库存接口
 *
 * @author lixin
 */
@Component
public interface StorageService {

    /**
     * 扣减库存
     *
     * @param id    库存ID
     * @param count 扣减数量
     */
    void decrease(Long id, Integer count);

}
