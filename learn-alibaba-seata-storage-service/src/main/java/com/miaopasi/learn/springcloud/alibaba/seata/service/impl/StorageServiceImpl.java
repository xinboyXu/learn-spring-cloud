package com.miaopasi.learn.springcloud.alibaba.seata.service.impl;

import com.miaopasi.learn.springcloud.alibaba.seata.mapper.StorageMapper;
import com.miaopasi.learn.springcloud.alibaba.seata.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 库存操作实现类
 *
 * @author lixin
 */
@Service
public class StorageServiceImpl implements StorageService {

    private final StorageMapper storageMapper;

    @Autowired
    public StorageServiceImpl(StorageMapper storageMapper) {
        this.storageMapper = storageMapper;
    }

    @Override
    public void decrease(Long id, Integer count) {
        storageMapper.decrease(id, count);
    }
}
