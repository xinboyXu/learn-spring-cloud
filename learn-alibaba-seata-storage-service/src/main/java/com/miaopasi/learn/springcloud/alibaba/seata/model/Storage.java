package com.miaopasi.learn.springcloud.alibaba.seata.model;

import java.io.Serializable;

/**
 * 库存实体类
 * @author lixin
 */
public class Storage implements Serializable {
    private Long id;
    private Long productId;
    private Integer total;
    private Integer used;
    private Integer residue;

    public Storage() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getUsed() {
        return used;
    }

    public void setUsed(Integer used) {
        this.used = used;
    }

    public Integer getResidue() {
        return residue;
    }

    public void setResidue(Integer residue) {
        this.residue = residue;
    }


    public static final class StorageBuilder {
        private final Storage storage;

        private StorageBuilder() {
            storage = new Storage();
        }

        public static StorageBuilder aStorage() {
            return new StorageBuilder();
        }

        public StorageBuilder id(Long id) {
            storage.setId(id);
            return this;
        }

        public StorageBuilder productId(Long productId) {
            storage.setProductId(productId);
            return this;
        }

        public StorageBuilder total(Integer total) {
            storage.setTotal(total);
            return this;
        }

        public StorageBuilder used(Integer used) {
            storage.setUsed(used);
            return this;
        }

        public StorageBuilder residue(Integer residue) {
            storage.setResidue(residue);
            return this;
        }

        public Storage build() {
            return storage;
        }
    }
}
