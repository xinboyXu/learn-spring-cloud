package com.miaopasi.learn.springcloud.alibaba.seata.controller;

import com.miaopasi.learn.springcloud.alibaba.seata.service.StorageService;
import com.miaopasi.learn.springcloud.common.pojo.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单控制层
 *
 * @author lixin
 */
@RestController
public class StorageController {

    private final StorageService storageService;

    @Autowired
    public StorageController(StorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping("/storage/decrease")
    public CommonResult<Boolean> decrease(
            @RequestParam("productId") Long productId,
            @RequestParam("count") Integer count
    ) {
        storageService.decrease(productId, count);
        return CommonResult.ok(Boolean.TRUE);
    }
}
