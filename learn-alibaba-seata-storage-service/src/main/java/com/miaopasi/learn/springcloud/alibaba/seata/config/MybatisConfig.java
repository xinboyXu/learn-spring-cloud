package com.miaopasi.learn.springcloud.alibaba.seata.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Mybatis配置扫描mapper
 *
 * @author lixin
 */
@Configuration
@MapperScan("com.miaopasi.learn.springcloud.alibaba.seata.mapper")
public class MybatisConfig {
}
