package com.miaopasi.learn.springcloud.provider.service;

import com.miaopasi.learn.springcloud.common.pojo.Dept;

import java.io.Serializable;

/**
 * 操作接口
 *
 * @author lixin
 */
public interface DeptService {

    /**
     * 根据id获取记录信息
     *
     * @param id 记录ID
     * @return 部门信息
     */
    Dept get(Serializable id);
}
