package com.miaopasi.learn.springcloud.provider.service.impl;

import com.miaopasi.learn.springcloud.common.pojo.Dept;
import com.miaopasi.learn.springcloud.provider.service.DeptService;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * 接口实现类
 *
 * @author lixin
 */
@Service
public class DeptServiceImpl implements DeptService {
    @Override
    public Dept get(Serializable id) {
        return Dept.DeptBuilder.aDept()
                .id((Long) id)
                .name("技术部" + id)
                .database("10000")
                .build();
    }
}
