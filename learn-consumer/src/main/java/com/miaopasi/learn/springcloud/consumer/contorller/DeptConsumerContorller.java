package com.miaopasi.learn.springcloud.consumer.contorller;

import com.miaopasi.learn.springcloud.common.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * 部门消费者
 *
 * @author lixin
 */
@RestController
@RequestMapping("/consumer/dept")
public class DeptConsumerContorller {
    private final RestTemplate restTemplate;

    /*** 服务调用地址，provider01是注册在eureka里面的名称 */
    private final static String BASE_URL = "http://provider01";

    @Autowired
    public DeptConsumerContorller(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/get/{id}")
    public Dept get(@PathVariable Long id) {
        String url = BASE_URL + "/dept/get/" + id;
        ResponseEntity<Dept> forEntity = restTemplate.getForEntity(url, Dept.class);
        return forEntity.getBody();
    }
}
