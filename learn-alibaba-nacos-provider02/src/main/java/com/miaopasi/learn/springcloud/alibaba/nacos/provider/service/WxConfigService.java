package com.miaopasi.learn.springcloud.alibaba.nacos.provider.service;

import com.miaopasi.learn.springcloud.common.pojo.WxConfig;

/**
 * 微信配置接口
 * @author lixin
 */
public interface WxConfigService {

    /***
     * 根据微信appId找到微信信息
     * @param appId  微信appId
     * @return 微信信息
     */
    WxConfig findByAppId(String appId);

}
