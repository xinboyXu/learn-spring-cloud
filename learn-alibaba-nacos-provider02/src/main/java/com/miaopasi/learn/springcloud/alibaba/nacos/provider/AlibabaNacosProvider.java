package com.miaopasi.learn.springcloud.alibaba.nacos.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Nacos 服务消费者
 *
 * @author lixin
 */
@SpringBootApplication
@EnableDiscoveryClient
public class AlibabaNacosProvider {
    public static void main(String[] args) {
        SpringApplication.run(AlibabaNacosProvider.class, args);
    }
}
