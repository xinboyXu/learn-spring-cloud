package com.miaopasi.learn.springcloud.alibaba.sentinel.service;

import com.miaopasi.learn.springcloud.common.pojo.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 使用 Feign 远程调用 learn-alibaba-sentinel-service01
 *
 * @author lixin
 */
@Service
@FeignClient(value = "alibaba-sentinel-service01", fallback = TestFallbackFactoryImpl.class)
public interface TestService {

    /**
     * 远程调用 /get/fallback 接口
     *
     * @param id 参数
     * @return 调用结果
     */
    @GetMapping("/get/fallback")
    CommonResult<String> fallback(@RequestParam(name = "id") Integer id);

}
