package com.miaopasi.learn.springcloud.alibaba.sentinel.service;

import com.miaopasi.learn.springcloud.common.pojo.CommonResult;
import org.springframework.stereotype.Component;

/**
 * 降级类
 *
 * @author lixin
 */
@Component
public class TestFallbackFactoryImpl implements TestService {
    @Override
    public CommonResult<String> fallback(Integer id) {
        return CommonResult.fail(987L, "FeignClient服务降级返回信息：" + id);
    }
}
