package com.miaopasi.learn.springcloud.alibaba.sentinel.controller;

import com.miaopasi.learn.springcloud.alibaba.sentinel.service.TestService;
import com.miaopasi.learn.springcloud.common.pojo.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试类
 *
 * @author lixin
 */
@RestController
public class TestController {

    private final TestService testService;

    @Autowired
    public TestController(TestService testService) {
        this.testService = testService;
    }

    @GetMapping("/feign/get/fallback")
    public CommonResult<String> fallback(@RequestParam(name = "id") Integer id) {
        return testService.fallback(id);
    }

}
