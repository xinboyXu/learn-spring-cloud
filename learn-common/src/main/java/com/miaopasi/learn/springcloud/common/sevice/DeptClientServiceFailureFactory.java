package com.miaopasi.learn.springcloud.common.sevice;

import com.miaopasi.learn.springcloud.common.pojo.Dept;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 服务降级：指定服务被关闭时触发降级，会进入降级方法
 *
 * @author lixin
 */
@Component
public class DeptClientServiceFailureFactory implements FallbackFactory<DeptClientService> {

    @Override
    public DeptClientService create(Throwable cause) {
        return new DeptClientService() {
            @Override
            public Dept get(Serializable id) {
                return Dept.DeptBuilder.aDept()
                        .id((Long) id)
                        .name("发生服务熔断，返回缺省值-1")
                        .database("null")
                        .build();
            }
        };
    }
}
