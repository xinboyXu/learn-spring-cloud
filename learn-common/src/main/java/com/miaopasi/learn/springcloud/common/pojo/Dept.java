package com.miaopasi.learn.springcloud.common.pojo;

import java.io.Serializable;

/**
 * 部门实体类
 *
 * @author lixin
 */
public class Dept implements Serializable {

    private Long id;
    private String name;
    private String database;

    public Dept() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }


    public static final class DeptBuilder {
        private Long id;
        private String name;
        private String database;

        private DeptBuilder() {
        }

        public static DeptBuilder aDept() {
            return new DeptBuilder();
        }

        public DeptBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public DeptBuilder name(String name) {
            this.name = name;
            return this;
        }

        public DeptBuilder database(String database) {
            this.database = database;
            return this;
        }

        public Dept build() {
            Dept dept = new Dept();
            dept.setId(id);
            dept.setName(name);
            dept.setDatabase(database);
            return dept;
        }
    }
}
