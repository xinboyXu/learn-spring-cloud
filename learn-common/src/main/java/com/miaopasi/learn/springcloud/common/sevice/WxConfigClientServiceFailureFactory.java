package com.miaopasi.learn.springcloud.common.sevice;

import com.miaopasi.learn.springcloud.common.pojo.WxConfig;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 服务降级：指定服务被关闭时触发降级，会进入降级方法
 *
 * @author lixin
 */
@Component
public class WxConfigClientServiceFailureFactory implements FallbackFactory<WxConfigClientService> {

    @Override
    public WxConfigClientService create(Throwable cause) {
        return new WxConfigClientService() {
            @Override
            public WxConfig findByAppId(String appId) {
                return WxConfig.WxConfigBuilder
                        .aWxConfig()
                        .appId(appId)
                        .name("发生服务熔断，返回缺省值-1")
                        .build();
            }
        };
    }
}
