package com.miaopasi.learn.springcloud.common.sevice;

import com.miaopasi.learn.springcloud.common.pojo.WxConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/***
 * 微信配置接口
 * @author lixin
 */
@Component
@FeignClient(value = "zipkin-provider", fallbackFactory = WxConfigClientServiceFailureFactory.class)
public interface WxConfigClientService {

    /***
     * 根据微信appId找到微信信息
     * @param appId  微信appId
     * @return 微信信息
     */
    @GetMapping("/zipkin/provider/wxConfig/find")
    WxConfig findByAppId(@RequestParam(name = "appId", required = false, defaultValue = "") String appId);

}
