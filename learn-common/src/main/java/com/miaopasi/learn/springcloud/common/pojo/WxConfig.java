package com.miaopasi.learn.springcloud.common.pojo;

import java.io.Serializable;

/**
 * 微信配置实体
 *
 * @author lixin
 */
public class WxConfig implements Serializable {

    private String appId;
    private String name;

    public WxConfig() {
    }

    public WxConfig(String appId, String name) {
        this.appId = appId;
        this.name = name;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public static final class WxConfigBuilder {
        private String appId;
        private String name;

        private WxConfigBuilder() {
        }

        public static WxConfigBuilder aWxConfig() {
            return new WxConfigBuilder();
        }

        public WxConfigBuilder appId(String appId) {
            this.appId = appId;
            return this;
        }

        public WxConfigBuilder name(String name) {
            this.name = name;
            return this;
        }

        public WxConfig build() {
            WxConfig wxConfig = new WxConfig();
            wxConfig.setAppId(appId);
            wxConfig.setName(name);
            return wxConfig;
        }
    }
}
