package com.miaopasi.learn.springcloud.common.sevice;

import com.miaopasi.learn.springcloud.common.pojo.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.Serializable;


/**
 * 部门通用 操作接口
 * 使用Feign客户端去微服务中provider01获取
 *
 * @author lixin
 */
@Component
@FeignClient(value = "provider01", fallbackFactory = DeptClientServiceFailureFactory.class)
public interface DeptClientService {

    /**
     * 根据id获取记录信息
     *
     * @param id 记录ID
     * @return 部门信息
     */
    @GetMapping("/dept/get/{id}")
    Dept get(@PathVariable Serializable id);
}
