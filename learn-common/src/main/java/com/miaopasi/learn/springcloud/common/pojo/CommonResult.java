package com.miaopasi.learn.springcloud.common.pojo;

import cn.hutool.core.date.DateUtil;

import java.io.Serializable;

/**
 * 通用结果集
 *
 * @author lixin
 */
public class CommonResult<T> implements Serializable {
    /*** 是否成功 */
    private boolean success = false;
    /*** 错误提示(成功succeed) */
    private String message;
    /*** 处理成功返回的业务数据 */
    private T data;
    /*** 错误代码 */
    private Long code;
    /*** 响应时间 */
    private String timestamp;

    public static <T> CommonResult<T> ok(T data) {
        CommonResult<T> result = new CommonResult<T>();
        result.setCode(0L);
        result.setSuccess(Boolean.TRUE);
        result.setMessage("success");
        result.setTimestamp(DateUtil.now());
        result.setData(data);
        return result;
    }

    public static CommonResult<String> fail(Long code, String message) {
        CommonResult<String> result = new CommonResult<>();
        result.setCode(code);
        result.setMessage(message);
        result.setSuccess(Boolean.FALSE);
        result.setTimestamp(DateUtil.now());
        return result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
