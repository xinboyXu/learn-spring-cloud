package com.miaopasi.learn.springcloud.alibaba.nacos.provider.controller;

import com.miaopasi.learn.springcloud.common.pojo.WxConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * 微信配置类控制层
 *
 * @author lixin
 */
@RestController
@RequestMapping("/alibaba/consumer/wxConfig")
public class WxConfigController {

    private final RestTemplate restTemplate;

    @Value("${service.url.nacos-wx-config-service}")
    public String serviceUrl;

    @Autowired
    public WxConfigController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("find")
    public WxConfig find(@RequestParam(name = "appId", required = false, defaultValue = "") String appId) {
        String url = serviceUrl + "/alibaba/provider/wxConfig/find?appId=" + appId;
        return restTemplate.getForEntity(url, WxConfig.class).getBody();
    }

}
