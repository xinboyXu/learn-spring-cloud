package com.miaopasi.learn.springcloud.alibaba.sentinel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Sentinel测试启动类
 *
 * @author lixin
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SentinelService01Main {
    public static void main(String[] args) {
        SpringApplication.run(SentinelService01Main.class, args);
    }
}
