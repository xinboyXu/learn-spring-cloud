package com.miaopasi.learn.springcloud.alibaba.sentinel.controller;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.miaopasi.learn.springcloud.common.pojo.CommonResult;

/**
 * 自定义处理限流逻辑
 *
 * @author lixin
 */
public class CustomBlockHandler {

    public static CommonResult<String> handler01(BlockException exception) {
        return CommonResult.fail(444L, "触发sentinel自定义限流handler01：" + exception.getRuleLimitApp());
    }

    public static CommonResult<String> handlerFallback(Throwable e) {
        return CommonResult.fail(445L, "触发sentinel自定义降级handlerFallback：" + e.getMessage());
    }

}
