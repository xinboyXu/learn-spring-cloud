package com.miaopasi.learn.springcloud.consumer.feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * 服务消费者启动类
 *
 * @author lixin
 */
@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = "com.miaopasi.learn.springcloud")
// 如果FeignClients在其他包下，应该配置一下basePackages
@EnableFeignClients(basePackages = {"com.miaopasi.learn.springcloud"})
// 开启Hystrix监控页面
@EnableHystrixDashboard
public class ConsumerFeignMain {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerFeignMain.class, args);
    }

}
