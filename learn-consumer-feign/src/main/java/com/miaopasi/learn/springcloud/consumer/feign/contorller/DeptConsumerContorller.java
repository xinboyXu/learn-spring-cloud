package com.miaopasi.learn.springcloud.consumer.feign.contorller;

import com.miaopasi.learn.springcloud.common.pojo.Dept;
import com.miaopasi.learn.springcloud.common.sevice.DeptClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 部门消费者
 *
 * @author lixin
 */
@RestController
@RequestMapping("/consumer/feign/dept")
public class DeptConsumerContorller {

    private final DeptClientService deptClientService;

    @Autowired
    public DeptConsumerContorller(DeptClientService deptClientService) {
        this.deptClientService = deptClientService;
    }

    @GetMapping("/get/{id}")
    public Dept get(@PathVariable Long id) {
        return this.deptClientService.get(id);
    }
}
